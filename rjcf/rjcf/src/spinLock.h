
/*******************************************************************
 ** 文件名:	spinLock.h
 ** 创建人:	饶建超
 ** 日  期:	2015-06-27
 ** 版  本:	1.0
 ** 描  述:	自旋锁
 ** 应  用:
 **************************** 修改记录 ******************************
 ** 修改人:
 ** 日  期:
 ** 描  述:
 ********************************************************************/

#pragma once

#ifndef __linux

#include <intrin.h>     // _ReadWriteBarrier(), InterlockedCompareExchange()

extern "C" void _ReadWriteBarrier(void);
#pragma intrinsic(_ReadWriteBarrier)

#ifndef __sync_val_compare_and_swap
#define __sync_val_compare_and_swap(destPtr, oldValue, newValue)        \
InterlockedCompareExchange((volatile LONG *)(destPtr),              \
(LONG)(newValue), (LONG)(oldValue))
#endif

#ifndef __sync_bool_compare_and_swap
#define __sync_bool_compare_and_swap(destPtr, oldValue, newValue)       \
(InterlockedCompareExchange((volatile LONG *)(destPtr),             \
(LONG)(newValue), (LONG)(oldValue))         \
== (LONG)(oldValue))
#endif

typedef   Sleep   sleep

#else
#include <pthread.h>
#include <unistd.h>
#endif /* __linux */

// 精简自旋锁
struct spin_mutex_s
{
#define SLEEP_SPIN_TIME		50				// 定义自旋次数
    enum _SpinState
    {
        _StateUnLock	= 0,
        _StateLock		= 1,
        _StateSleep		= SLEEP_SPIN_TIME,
    };
    //*/
    spin_mutex_s():spinState(_StateUnLock){};
    spin_mutex_s(size_t n):spinState(_StateUnLock){};
    void UnLock()		// 解锁
    {
        spinState = _StateUnLock;
        //_ReadWriteBarrier();
    }
    void Lock()			// 加锁
    {
        while(!TryGetLock()){}
    }
    void LockImmediately()	// 立即加锁
    {
        spinState = _StateLock;
        //_ReadWriteBarrier();
    }
    bool isLocked()		// 是否已锁
    {
        return spinState != _StateUnLock;
    }
    
    // bSleepImmed: false(失败后尝试N次再sleep) true(失败就sleep)
    bool TryGetLock(bool bSleepImmed = true)	// 尝试获得锁
    {
        bool bWriteSuc = false;
        while(!bWriteSuc)
        {
            size_t tStateTemp = spinState;
            if (tStateTemp == _StateUnLock)
            {
                //_ReadWriteBarrier();
                bWriteSuc = __sync_bool_compare_and_swap(&spinState, tStateTemp, _StateLock);
                if (bWriteSuc)
                {
                    //_ReadWriteBarrier();
                    return true;
                }
            }
            else if (bSleepImmed || tStateTemp >= _StateSleep)
            {
                bWriteSuc = __sync_bool_compare_and_swap(&spinState, tStateTemp, _StateLock);
                if (bWriteSuc)
                {
                    sleep(1);
                    return false;
                }
            }
            else
            {
                bWriteSuc = __sync_bool_compare_and_swap(&spinState, tStateTemp, tStateTemp+1);
                if (bWriteSuc)
                {
                    return false;
                }
            }
        }
        return false;
    }
    
protected:
    volatile size_t spinState;
};
