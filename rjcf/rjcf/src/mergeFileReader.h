//
//  mergeFileReader.h
//  rjcf
//
//  Created by admin on 15-7-7.
//  Copyright (c) 2015年 admin. All rights reserved.
//

#ifndef __rjcf__mergeFileReader__
#define __rjcf__mergeFileReader__

#include <stdio.h>
#include "Singleton.h"
#include "rjcfDefine.h"
#include "spinLock.h"

using namespace rkt;

#define FILE_HANDLE_MAX     20

class CMergeFileReader: public SingletonEx<CMergeFileReader>
{
public:
    typedef map<FILE *, bool>   MAP_FILE_NOTE;
    CMergeFileReader(string strFilePath)
    {
        initMergeFile(strFilePath);
    }
    ~CMergeFileReader(){
        clear();
    }
public:
    // 得到文件数据操作句柄
    FILE * open(string strFilePath, stFileMsg &stTemp);
    
    // 释放句柄使用权
    void close(FILE * fp);
    
private:
    // 初始化，载入打包文件
    bool initMergeFile(string strMergeFile);
    
    // 尝试获取一个可用句柄
    FILE * getFileHandle();
    
    // 查找一个文件是否存在，并返回其文件索引信息
    bool findFileMsg(const string strPath, stFileMsg &stMsg);
    
    // string 将所有大写字符转成小写
    string& tolower(string & s);
    
    // 清空数据
    void clear();
    
public:
    HASH_MSG        m_mapFileMsg;       // 文件索引表
    stFileHead      m_stFileHeader;     // 文件头数据
    MAP_FILE_NOTE   m_mapFilePool;      // 文件读取句柄池
    string          m_strMergeFile;     // 打包文件路径
    spin_mutex_s    m_spinLock;         // 互斥自旋锁
};

#endif /* defined(__rjcf__mergeFileReader__) */
