//
//  mergeFileReader.cpp
//  rjcf
//
//  Created by admin on 15-7-7.
//  Copyright (c) 2015年 admin. All rights reserved.
//

#include <iostream>
#include "mergeFileReader.h"
#include "fileHandle.h"


// 初始化，载入打包文件
bool CMergeFileReader::initMergeFile(string strMergeFile)
{
    // 得到文件大小
    long nSize = getFileSize(strMergeFile.c_str());
    FILE * pfTemp = fopen(strMergeFile.c_str(), "rb");
    if (pfTemp != NULL )
    {
        m_strMergeFile = strMergeFile;
        clear();
        
        if (nSize < sizeof(stFileHead)) {
            return false;
        }
        m_mapFileMsg.clear();
        char chrPathTemp[MAX_PATH] = {};
        stFileMsg  stFileTemp;
        fread(&m_stFileHeader, 1, sizeof(m_stFileHeader), pfTemp);
        for (size_t i=0; i<m_stFileHeader.nFileNum; ++i)
        {
            fread(&stFileTemp, 1, sizeof(stFileTemp), pfTemp);
            if (stFileTemp.nPathLen >= MAX_PATH)
            {
                cout<<"异常：发现文件路径字符串过长(大于"<<MAX_PATH<<")："<<stFileTemp.nPathLen<<endl;
                return false;
            }
            fread(chrPathTemp, 1, stFileTemp.nPathLen, pfTemp);
            chrPathTemp[stFileTemp.nPathLen] = 0;
            m_mapFileMsg[string(chrPathTemp)] = stFileTemp;
           
            // show log: for test
            cout<<"path:"<<chrPathTemp<<" st.nPathLen="<<stFileTemp.nPathLen
            <<" st.nOffset="<<stFileTemp.nOffset
            <<" st.nBuffSize="<<stFileTemp.nBuffSize<<endl;
            //*/
        }
        m_mapFilePool[pfTemp] = true;
    }
    else
    {
        cout<<"打开打包文件失败。"<<endl;
        return false;
    }
    return true;
}

// 得到文件数据操作句柄
FILE * CMergeFileReader::open(string strFilePath, stFileMsg &stTemp)
{
    tolower(strFilePath);
    if(!findFileMsg(strFilePath, stTemp))
    {
        return NULL;
    }
    FILE * pf = getFileHandle();
    /*while (pf == NULL)
    {
        sleep(1);
        pf = getFileHandle();
    }
    //*/
    if (pf == NULL)
    {
        return NULL;
    }
    //*/
    int nFlag = fseek(pf, stTemp.nOffset, SEEK_SET);
    if (nFlag != 0) {
        fclose(pf);
        return NULL;
    }
    
    return pf;
}

// 释放句柄使用权
void CMergeFileReader::close(FILE * fp)
{
    m_spinLock.Lock();
    MAP_FILE_NOTE::iterator iter = m_mapFilePool.find(fp);
    if (iter != m_mapFilePool.end())
    {
        // 设为可用
        iter->second = true;
    }
    else
    {
        if (fp != NULL)
            fclose(fp);
    }
    m_spinLock.UnLock();
}

// 尝试获取一个可用句柄
FILE * CMergeFileReader::getFileHandle()
{
    // 此处需加锁，以确保不会多个线程同时用一个句柄
    m_spinLock.Lock();
    MAP_FILE_NOTE::iterator iter = m_mapFilePool.begin();
    for (; iter != m_mapFilePool.end(); ++iter)
    {
        if (iter->second)
        {
            iter->second = false;    // 设为不可用
            m_spinLock.UnLock();
            return iter->first;
        }
    }
    if (m_mapFilePool.size() < FILE_HANDLE_MAX && iter == m_mapFilePool.end())
    {
        FILE * pf = fopen(m_strMergeFile.c_str(), "rb");
        if (pf != NULL)
        {
            m_mapFilePool[pf] = false;
            m_spinLock.UnLock();
            return pf;
        }
    }
    m_spinLock.UnLock();
    return NULL;
}

// 查找一个文件是否存在，并返回其文件索引信息
bool CMergeFileReader::findFileMsg(string strPath, stFileMsg &stMsg)
{
    HASH_MSG::iterator iter = m_mapFileMsg.find(strPath);
    if (iter == m_mapFileMsg.end())
    {
        return false;
    }
    // for test
    /*cout<<"size:"<<m_mapFileMsg.size()<<endl;
    iter = m_mapFileMsg.begin();
    while (iter != m_mapFileMsg.end())
    {
        cout<<iter->first<<endl;
        ++iter;
    }
    //*/
    
    stMsg = iter->second;
    return true;
}

// 清空数据
void CMergeFileReader::clear()
{
    m_spinLock.Lock();
    MAP_FILE_NOTE::iterator iter = m_mapFilePool.begin();
    for (; iter != m_mapFilePool.end(); ++iter)
    {
        if (iter->second)
        {
            fclose(iter->first);
        }
    }
    m_mapFilePool.clear();
    m_spinLock.UnLock();
}

// string 将所有大写字符转成小写
string& CMergeFileReader::tolower(string & s)
{
    if (s.empty()) {
        return s;
    }
    size_t nLength = s.size();
    char * pchr = new char[nLength + 1];
    memcpy(pchr, s.c_str(), s.size()+1);
    for (size_t i=0; i<s.size(); ++i)
    {
        if (*(pchr+i) >= 'A' && *(pchr+i) <= 'Z')
            *(pchr+i) += 32;
    }
    s = pchr;
    delete []pchr; pchr = NULL;
    return s;
}

