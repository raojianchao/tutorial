//
//  fileHandle.h
//  rjcf
//
//  Created by admin on 15-7-8.
//  Copyright (c) 2015年 admin. All rights reserved.
//

#ifndef __rjcf__fileHandle__
#define __rjcf__fileHandle__

#include <stdio.h>
#include "rjcfDefine.h"

#ifdef __linux
#include <fstream>
#include <dirent.h>
#include <unistd.h>
#include <vector>
#include <sys/stat.h>
#define  MAX_PATH        512
#else
#endif /* defined(__linux) */

// 指定路径是不是一个文件夹
EM_PATHTYPE getPathType(const char* pathname);

// 得到路径下的所有文件路径列表
// @return -1:查找失败, 0：查找成功
int         list_dir_name(string strdir, vector<string> &vctFilePath);

// 得到当前工作路径
int         getCurrentDir(int nLimitNum, char * lpPath);

// 得到文件大小
long        getFileSize(const char *filename);


#endif /* defined(__rjcf__fileHandle__) */
