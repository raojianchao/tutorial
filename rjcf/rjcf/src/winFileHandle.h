
#pragma once

#ifndef __linux

#include "rjcfDefine.h"
#include <windows.h>
#include <string>
#include <CString>

BOOL FindFirstFileExists(LPCTSTR lpPath, DWORD dwFilter)
{
    WIN32_FIND_DATA fd;
    HANDLE hFind = FindFirstFile(lpPath, &fd);
    BOOL bFilter = (FALSE == dwFilter) ? TRUE : fd.dwFileAttributes & dwFilter;
    BOOL RetValue = ((hFind != INVALID_HANDLE_VALUE) && bFilter) ? TRUE : FALSE;
    FindClose(hFind);
    return RetValue;
}

// 指定路径是什么类型
EM_PATHTYPE GetPathType(const char* pathname)
{
    if ( !FindFirstFileExists((LPCTSTR)(LPTSTR)pathname, FALSE))
    {
        return EM_TYPE_NOT_EXIST;
    }
    else
    {
        if (FindFirstFileExists((LPCTSTR)(LPTSTR)pathname, FILE_ATTRIBUTE_DIRECTORY))
        {
            return EM_TYPE_IS_POLDER;
        }
        else
        {
            return EM_TYPE_IS_FILE;
        }
    }
}

// 得到路径下的所有文件路径列表
// @return -1:查找失败, 0：查找成功
int list_dir_name(string strdir, vector<string> &vctFilePath, size_t nMaxNum = MAX_FILE_NUM)
{
    if (strdir.size() <= 1)
    {
        return -1;
    }
    char szFind[MAX_PATH];
    char szFileName[MAX_PATH];
    WIN32_FIND_DATA FindFileData;
    
    strcpy_s(szFind,strdir.c_str());
    strcat_s(szFind,"\\*.*");
    
    HANDLE hFind=::FindFirstFile((LPCTSTR)(LPTSTR)szFind, &FindFileData);
    if(INVALID_HANDLE_VALUE == hFind)
    {
        return -1;
    }
    while(TRUE)
    {
        if(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
        {
            if(FindFileData.cFileName[0]!='.')
            {
                char szFile[MAX_PATH];
                strcpy_s(szFile,strdir.c_str());
                strcat_s(szFile,"\\");
                strcat_s(szFile,(char *)FindFileData.cFileName);
                list_dir_name(szFile, vctFilePath);
            }
        }
        else
        {
            sprintf_s(szFileName,"%s\\%s", strdir.c_str(), (CHAR*)(LPCTSTR)FindFileData.cFileName);
            vctFilePath.push_back((szFileName));
            if (nMaxNum <= vctFilePath.size())
            {
                return 1;
            }
        }
        if(!FindNextFile(hFind, &FindFileData))
            break;
    }
    FindClose(hFind);
    return 0;
}

// 得到当前工作路径
int GetCurrentDir(int nLimitNum, char *lpPath)
{
    return GetCurrentDirectory(nLimitNum, (LPTSTR)lpPath);
}

#endif