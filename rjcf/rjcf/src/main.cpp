//  main.cpp
//  rjcf
//
//  Created by admin on 15-7-7.
//  Copyright (c) 2015年 admin. All rights reserved.
//

#include <iostream>
#include <vector>

#include <stdio.h>
#include <string.h>
#include "rjcfDefine.h"

#include "fileMerger.h"
#include "mergeFileReader.h"
#include "fileHandle.h"

using namespace std;

// 显示帮助信息
void show_help()
{
    cout<<"帮助信息："<<endl;
    cout<<"help   显示帮助信息"<<endl;
    cout<<"rjcf   打包命令格式：rjcf strMergePath strFile1;strFile2;strFolder1;..."<<endl;
    cout<<"bye    退出程序"<<endl;
}

// string 去首尾的空格字符
string& trim(std::string &s)
{
    if (s.empty())
    {
        return s;
    }
    s.erase(0,s.find_first_not_of(" "));
    s.erase(s.find_last_not_of(" ") + 1);
    return s;
}

// 字符串分割函数
vector<std::string> split(string str,string pattern)
{
    string::size_type pos;
    vector<string> result;
    str += pattern;//扩展字符串以方便操作
    
    string::size_type size = str.size();
    for(string::size_type i=0; i<size; i++)
    {
        pos = str.find(pattern, i);
        if (pos < size)
        {
            std::string s = str.substr(i,pos-i);
            result.push_back(s);
            i = pos + pattern.size() - 1;
        }
    }
    return result;
}

// 进行打包
bool doPackage(string strMergePath, vector<string> vctSrcPath)
{
    vector<string> vctAllFiles;
    for (size_t i=0; i<vctSrcPath.size(); ++i)
    {
        trim(vctSrcPath[i]);            // 去掉字符串首尾空格
        if (vctSrcPath[i].size() > 0 )  // 路径不为空
        {
            EM_PATHTYPE nType = getPathType(vctSrcPath[i].c_str());
            if (nType == EM_TYPE_IS_POLDER)
            {
                list_dir_name(vctSrcPath[i], vctAllFiles);
            }
            else if (nType == EM_TYPE_IS_FILE)
            {
                vctAllFiles.push_back(vctSrcPath[i]);
            }
        }
    }
    if (vctAllFiles.size() > 0)
    {
        return CFileMerger::getInstance().packageFile(strMergePath, vctAllFiles);
    }
    cout<<"打包源文件列表为空，不能打包。"<<endl;
    return false;
}

// just for test func
void test()
{
    char szWorkDir[MAX_PATH] = {0} ;
    if (getCurrentDir(MAX_PATH, szWorkDir) == -1)
    {
        cout<<"获取工作路径失败"<<endl;
    }
    if (1)
    {
        cout<<"初始化成功！"<<endl;
        char chrBuff[BUFFER_SIZE_MAX] = {};
        stFileMsg stTemp;
        // 读文件1
        FILE * pfile = CMergeFileReader::getInstance().open("codeCounter/CodeCounter/Thread/Thread.h", stTemp);
        if (pfile != NULL) {
            int nSizeTemp = (int)stTemp.nBuffSize;
            FILE * pfWrite1 = fopen("test1.h", "wb");
            if (pfWrite1 != NULL)
            {
                while (nSizeTemp > 0 ) {
                    size_t nRead = fread(chrBuff, 1, MIN(nSizeTemp, BUFFER_SIZE_MAX), pfile);
                    fwrite(chrBuff, 1, nRead, pfWrite1);
                    nSizeTemp -= nRead;
                }
                if (nSizeTemp != 0)
                    cout<<"取数据有误！"<<endl;
                fclose(pfWrite1);
            }
            CMergeFileReader::getInstance().close(pfile);
        }
        // 读文件2
        FILE * pfile2 = CMergeFileReader::getInstance().open("foldTest/The Unarchiver.zip", stTemp);
        if (pfile2 != NULL) {
            int nSizeTemp = (int)stTemp.nBuffSize;
            FILE * pfWrite1 = fopen("test2.zip", "wb");
            if (pfWrite1 != NULL)
            {
                while (nSizeTemp > 0 ) {
                    size_t nRead = fread(chrBuff, 1, MIN(nSizeTemp, BUFFER_SIZE_MAX), pfile2);
                    fwrite(chrBuff, 1, nRead, pfWrite1);
                    nSizeTemp -= nRead;
                }
                if (nSizeTemp != 0)
                    cout<<"取数据有误！"<<endl;
                fclose(pfWrite1);
            }
            fread(chrBuff, 1, stTemp.nBuffSize<BUFFER_SIZE_MAX ? stTemp.nBuffSize : BUFFER_SIZE_MAX, pfile2);
            CMergeFileReader::getInstance().close(pfile2);
        }
    }
    
    cout<<szWorkDir<<endl;
}

int main(int argc, const char * argv[]) {
    // insert code here...
    new CFileMerger;
    new CMergeFileReader("code");
    
    show_help();
    string strIn;
    getline(cin, strIn);
    
    while(strIn != "bye")
    {
        // 去字符串首尾空格
        trim(strIn);
        if (strIn == "help")
        {
            show_help();
        }
        else if (strIn.find("rjcf ") == 0)
        {
            string strMergePath;
            vector<string> vctSrcPath;
            strIn = strIn.substr(5, strIn.size()-5);
            // 再次去掉字符串首尾空格
            trim(strIn);
            unsigned long n = strIn.find_first_of(" ");
            if (n != -1)
            {
                strMergePath = strIn.substr(0, n);
                cout<<"目标文件："<<strMergePath<<endl;
                strIn = strIn.substr(n, strIn.size()-n);
                // 再次去掉字符串首尾空格
                trim(strIn);
                if (strIn.size() > 0)
                {
                    vctSrcPath = split(strIn, ";");
                    // 进行打包
                    bool bSuc = doPackage(strMergePath, vctSrcPath);
                    if (bSuc) {
                        cout<<"打包成功！"<<endl;
                    }
                    break;
                    //continue;
                }
            }
            cout<<"异常命令，查看帮助请输入：help"<<endl;
        }
        else if(strIn == "test")
        {
            test();     // for test
        }
        else
        {
            cout<<"异常命令，查看帮助请输入：help"<<endl;
        }
        getline(cin, strIn);
    }
    
    return 0;
}
