//
//  fileHandle.cpp
//  rjcf
//
//  Created by admin on 15-7-8.
//  Copyright (c) 2015年 admin. All rights reserved.
//

#include "fileHandle.h"

#ifdef __linux

// 指定路径是不是一个文件夹
EM_PATHTYPE getPathType(const char* pathname)
{
    DIR* dp;
    if ((dp = opendir(pathname)) != NULL)
    {
        return EM_TYPE_IS_POLDER;
    }
    else
    {
        FILE* fp;
        fp = fopen(pathname, "r");
        if (fp == NULL )
        {
            return EM_TYPE_NOT_EXIST;
        }
        fclose(fp);
        return EM_TYPE_IS_FILE;
    }
    return EM_TYPE_NOT_EXIST;
}


// 得到路径下的所有文件路径列表
// @return -1:查找失败, 0：查找成功
int list_dir_name(string strdir, vector<string> &vctFilePath)
{
    DIR* dp;
    struct dirent* dirp;
    struct stat st;
    
    /* open dirent directory */
    if((dp = opendir(strdir.c_str())) == NULL)
    {
        perror("opendir");
        return -1;
    }
    
    // read all files in this dir
    while((dirp = readdir(dp)) != NULL)
    {
        string strfullname = strdir;
        
        /* ignore hidden files */
        if(dirp->d_name[0] == '.')
            continue;
        
        //
        if (strfullname.find_last_of("/\\") != strfullname.size()-1)
        {
            strfullname += "/";
        }
        strfullname += dirp->d_name;
        
        /* get dirent status */
        if(stat(strfullname.c_str(), &st) == -1)
        {
            perror("stat");
            fputs(strfullname.c_str(), stderr);
            return -1;
        }
        /* cann't open dirent directory is a file */
        if(getPathType(strfullname.c_str()) == EM_TYPE_IS_FILE)
        {
            vctFilePath.push_back(strfullname);
        }
        else
        {
            /* if dirent is a directory, call itself */
            if(S_ISDIR(st.st_mode) && list_dir_name(strfullname, vctFilePath) == -1)
                return -1;
        }
    }
    return 0;
}

// 得到当前工作路径
int getCurrentDir(int nLimitNum, char * lpPath)
{
    char *pTemp = getcwd(lpPath, nLimitNum);
    return pTemp == NULL ? -1 : 0;
}

// 得到文件大小
long getFileSize(const char *filename)
{
    struct stat f_stat;
    if (stat(filename, &f_stat) == -1) {
        return -1;
    }
    return (long)f_stat.st_size;
}


#else

#endif