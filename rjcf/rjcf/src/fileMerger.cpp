
//  fileMerger.cpp
//  rjcf
//
//  Created by admin on 15-7-7.
//  Copyright (c) 2015年 admin. All rights reserved.
//	合并文件类

#include <iostream>
#include "fileMerger.h"
#include "rjcfDefine.h"
#include "fileHandle.h"

// 打包文件
bool CFileMerger::packageFile(const string strDesPath, vector<string> &vctFilePath)
{
    // 清空文件列表
    clearFileList();
    for(size_t i=0; i<vctFilePath.size(); ++i)
    {
        addFileMsg(vctFilePath[i]);
    }
    return startMerge(strDesPath);
}

// 开始进行打包
bool CFileMerger::startMerge(const string strDesPath)
{
    // 创建方式打开此路径文件
    // wb 只写打开或新建一个二进制文件；只允许写数据。
    FILE * pDesFileHandle = fopen(strDesPath.c_str(), "wb");
    if (pDesFileHandle == NULL )
    {
        cout<<"开始生成失败！因为：生成文件无法创建成功。"<<endl;
        return false;
    }
    // 1. 文件列表数据预处理
    if(!fileListDataPre(pDesFileHandle))
    {
        cout<<"文件预处理失败。"<<endl;
        return false;
    };
    
    // 2. 计算安全验证码
    //caclSecurityCode();
    
    // 3. 文件数据正式打包
    if(!bPackageData(pDesFileHandle))
    {
        cout<<"文件据打包失败。"<<endl;
        return false;
    };
    // 结束
    fclose(pDesFileHandle);
    
    // 测试调用
    //forTest(strDesPath);
    
    //ofstream fDesFile(strDesPath.c_str(), ios::binary);
    //fDesFile.write((char *)&(iter->second), sizeof(iter->second));
    //fDesFile.write(iter->first.c_str(), iter->second.nPathLen);
    
    return true;
}

// 文件列表数据预处理
bool CFileMerger::fileListDataPre(FILE * fpDes)
{
    if (fpDes == NULL)
    {
        return false;
    }
    // 开始计算各文件相关数据
    m_mapFieHead.nFileNum = m_mapFileMsg.size();
    size_t nOffset = sizeof(m_mapFieHead);     // 偏移量
    HASH_MSG::iterator iter = m_mapFileMsg.begin();
    
    for ( ;iter != m_mapFileMsg.end(); ++iter)
    {
        nOffset += sizeof(iter->second) + iter->second.nPathLen;
    }
    for (iter = m_mapFileMsg.begin(); iter != m_mapFileMsg.end(); ++iter)
    {
        iter->second.nOffset = nOffset;
        nOffset += iter->second.nBuffSize;
    }
    return true;
}

// 文件数据正式打包
bool CFileMerger::bPackageData(FILE * fpDes)
{
    if (fpDes == NULL)
    {
        return false;
    }
    // 开始写文件信息头
    fwrite(&m_mapFieHead, 1, sizeof(m_mapFieHead), fpDes);
    for ( HASH_MSG::iterator iter = m_mapFileMsg.begin(); iter != m_mapFileMsg.end(); ++iter)
    {
        fwrite(&iter->second, 1, sizeof(stFileMsg), fpDes);
        fwrite(iter->first.c_str(), 1, iter->second.nPathLen, fpDes);
    }
    // 开始写文件buffer
    char chrBufferRead[BUFFER_SIZE_MAX] = {};
    for (HASH_MSG::iterator iter = m_mapFileMsg.begin(); iter != m_mapFileMsg.end(); ++iter)
    {
        FILE * pf = fopen(iter->first.c_str(), "rb");
        if (pf == NULL)
        {
            cout<<"异常：不存在文件："<<iter->first<<endl;
            return false;
        }
        int nFileSize = (int)iter->second.nBuffSize;
        //memset(chrBufferRead, 0, sizeof(chrBufferRead));
        while (nFileSize > 0)
        {
            int nTemp = (int)fread(chrBufferRead, 1, BUFFER_SIZE_MAX, pf);
            fwrite(chrBufferRead, 1, nTemp, fpDes);
            nFileSize -= nTemp;
        }
        if (nFileSize != 0)
        {
            cout<<"file:"<<iter->first<<" error left:"<<nFileSize<<endl;
        }
        else
        {
            cout<<"file:"<<iter->first<<" copyed"<<endl;
        }
        fclose(pf);
    }
    //*/
    return true;
}

// 计算验证码
// nType:CRC?MD4?MD5?sha1
void CFileMerger::caclSecurityCode(int nType, string &strSecurCode)
{
    
}

// 查找一个文件是否存在，并返回其文件索引信息
bool CFileMerger::findFileMsg(const string strPath, stFileMsg &stMsg)
{
	HASH_MSG::iterator iter = m_mapFileMsg.find(strPath);
	if (iter == m_mapFileMsg.end())
	{
		stMsg = iter->second;
        return false;
	}
    // for test
    cout<<"size:"<<m_mapFileMsg.size()<<endl;
    while (iter != m_mapFileMsg.end())
    {
        cout<<iter->first<<endl;
        ++iter;
    }
    //*/
	return true;
}

bool CFileMerger::addFileMsg(string strPath)
{
    tolower(strPath);
    // 查找指定路径是否已存在
	HASH_MSG::iterator iter = m_mapFileMsg.find(strPath);
	if (iter != m_mapFileMsg.end())
	{
		return false;
	}
    
    // 查找指定路径是否是一个文件
    if (getPathType(strPath.c_str()) != EM_TYPE_IS_FILE)
    {
        return false;
    }
    
    // 得到文件大小
    long nSize = getFileSize(strPath.c_str());
    //cout<<"文件大小;"<<nSize<<endl;
    
	stFileMsg stTemp;
    stTemp.nBuffSize = nSize;
    stTemp.nPathLen = strPath.length();
    stTemp.nOffset = 0;
	m_mapFileMsg[strPath] = stTemp;
	return true;
}

// 得到打包文件总数
unsigned long CFileMerger::getFileNum()
{
    return m_mapFileMsg.size();
}

// 清空打包文件列表
void CFileMerger::clearFileList()
{
    m_mapFileMsg.clear();
}

void CFileMerger::forTest(const string strDesPath)
{
    // for test 解码过程
    // 得到文件大小
    long nSize = getFileSize(strDesPath.c_str());
    FILE * pDesFileTest = fopen(strDesPath.c_str(), "rb");
    if (pDesFileTest != NULL )
    {
        if (nSize < sizeof(stFileHead)) {
            fclose(pDesFileTest);
            return;
        }
        char chrPathTemp[MAX_PATH] = {};
        char chrBufferRead[BUFFER_SIZE_MAX] = {};
        stFileMsg  stFileTemp;
        stFileHead stHeadTemp;
        //HASH_MSG mapFileMsg;
        vector<stFileMsg> vctFileMsg;
        vector<string> vctFilePath;
        fread(&stHeadTemp, 1, sizeof(stHeadTemp), pDesFileTest);
        for (size_t i=0; i<stHeadTemp.nFileNum; ++i)
        {
            fread(&stFileTemp, 1, sizeof(stFileTemp), pDesFileTest);
            //memset(chrPathTemp, 0, sizeof(chrPathTemp));
            if (stFileTemp.nPathLen >= MAX_PATH)
            {
                cout<<"异常：发现文件路径字符串过长(大于"<<MAX_PATH<<")："<<stFileTemp.nPathLen<<endl;
                break;
            }
            fread(chrPathTemp, 1, stFileTemp.nPathLen, pDesFileTest);
            chrPathTemp[stFileTemp.nPathLen] = 0;
            //mapFileMsg[string(chrPathTemp)] = stFileTemp;
            vctFileMsg.push_back(stFileTemp);
            vctFilePath.push_back(string(chrPathTemp) + "t");
            
            // show log:
            cout<<"path:"<<chrPathTemp<<" st.nPathLen="<<stFileTemp.nPathLen
            <<" st.nOffset="<<stFileTemp.nOffset
            <<" st.nBuffSize="<<stFileTemp.nBuffSize<<endl;
        }
        
        // 重新生成各个文件
        for (size_t i=0; i<vctFileMsg.size(); ++i)
        {
            FILE * pFileTest = fopen(vctFilePath[i].c_str(), "wb");
            if (pFileTest != NULL)
            {
                int nFileLeft = (int)vctFileMsg[i].nBuffSize;
                while (nFileLeft > 0)
                {
                    int nBuffNum = (int)fread(chrBufferRead, 1, min(nFileLeft, BUFFER_SIZE_MAX), pDesFileTest);
                    if (nBuffNum != 0)
                    {
                        fwrite(chrBufferRead, 1, nBuffNum, pFileTest);
                        nFileLeft -= nBuffNum;
                    }
                    else
                    {
                        break;
                    }
                }
                if (nFileLeft != 0)
                {
                    cout<<"异常：left buff size:"<<nFileLeft<<endl;
                }
                fclose(pFileTest);
            }
            else
            {
                cout<<"异常：文件test打开失败。"<<endl;
            }
        }
        fclose(pDesFileTest);
        //*/
    }
}

// string 将所有大写字符转成小写
string& CFileMerger::tolower(string & s)
{
    if (s.empty()) {
        return s;
    }
    size_t nLength = s.size();
    char * pchr = new char[nLength + 1];
    memcpy(pchr, s.c_str(), s.size()+1);
    for (size_t i=0; i<s.size(); ++i)
    {
        if (*(pchr+i) >= 'A' && *(pchr+i) <= 'Z')
            *(pchr+i) += 32;
    }
    s = pchr;
    delete []pchr; pchr = NULL;
    return s;
}

