//  fileMerger.h
//  rjcf
//
//  Created by admin on 15-7-7.
//  Copyright (c) 2015年 admin. All rights reserved.
//
//  说明：文件打包类（单例模式）

#ifndef __rjcf__fileMerger__
#define __rjcf__fileMerger__

#include <stdio.h>
#include "rjcfDefine.h"
#include "Singleton.h"
#include <vector>

using namespace rkt;
using namespace std;

class CFileMerger: public SingletonEx<CFileMerger>
{
public:
    // 打包文件
    bool packageFile(const string strDesPath, vector<string> &vctFilePath);
    
private:
    // 开始进行打包
    bool startMerge(const string strDesPath);
    
    // 查找一个文件是否存在，并返回其文件索引信息
    bool findFileMsg(const string strPath, stFileMsg &stMsg);
    
    // 新增一个打包文件
    bool addFileMsg(string strPath);
    
    // 得到打包文件总数
    unsigned long getFileNum();
    
    // 清空打包文件列表
    void clearFileList();
    
    // 文件列表数据预处理
    bool fileListDataPre(FILE * fpDes);
    
    // 文件数据正式打包
    bool bPackageData(FILE * fpDes);
    
    // 计算验证码
    // nType:CRC?MD4?MD5?sha1
    void caclSecurityCode(int nType, string &strSecurCode);
    
    // string 将所有大写字符转成小写
    string& tolower(string & s);
    
    // 测试用函数
    void forTest(const string strDesPath);
    
private:
    stFileHead  m_mapFieHead;       // 文件头数据
    HASH_MSG    m_mapFileMsg;       // 打包文件索引表
};

#endif /* defined(__rjcf__fileMerger__) */
