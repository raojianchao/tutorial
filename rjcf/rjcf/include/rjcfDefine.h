//
//  rjcfDefine.h
//  rjcf
//
//  Created by admin on 15-7-7.
//  Copyright (c) 2015年 admin. All rights reserved.
//

#ifndef __rjcf__rjcfDefine__
#define __rjcf__rjcfDefine__

#include <stdio.h>
#include <map>

#define __linux

// hash_map 头文件
#ifdef __linux
#include <ext/hash_map>
#else
#include <hash_map>
#endif

using namespace std;

#ifdef __linux
using namespace __gnu_cxx;
#endif

#define BUFFER_SIZE_MAX     32768       // 32K
#define MIN(x, y)           ((x)>(y) ? (y) : (x))

// 文件类型
enum EM_PATHTYPE
{
    EM_TYPE_NOT_EXIST = 0,       // 不存在（不是文件也不是文件夹）
    EM_TYPE_IS_POLDER,           // 文件夹
    EM_TYPE_IS_FILE,             // 文件
    EM_TYPE_MAX,
};

// 打包文件的文件头信息
struct stFileHead
{
    char chrType[4];    // 固定文件头标志（rjcf）
    size_t nFileNum;    // 打包文件总数量
    
    stFileHead()
    {
        memcpy(chrType, "rjcf", sizeof(chrType));   // 默认填写：“rjcf”
        nFileNum = 0;
    }
};

// 每个文件索引相关信息
struct stFileMsg
{
    size_t nPathLen;		// 文件路径长度
    size_t nOffset;         // 文件起始偏移量
    size_t nBuffSize;		// 文件大小
};

//typedef hash_map<string, stFileMsg> HASH_MSG;
typedef map<string, stFileMsg> HASH_MSG;

#endif /* defined(__rjcf__rjcfDefine__) */
